package ru.crud.first;
import ru.crud.first.command.Command;
import ru.crud.first.repository.BinRepository;
import ru.crud.first.repository.ProductRepository;
import ru.crud.first.service.BinService;
import ru.crud.first.service.ProductService;

import java.util.Scanner;

public class Bootstrap {

    public void setBinService(BinService binService) {
        this.binService = binService;
    }
    public void setBinRepository(BinRepository binRepository) {
        this.binRepository = binRepository;
    }
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }
    public void setProductRepository(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    private BinService binService; private ProductService productService;
    private BinRepository binRepository; private ProductRepository productRepository;

    public void init(){
        setProductRepository(new ProductRepository());
        setProductService(new ProductService(productRepository));
        setBinRepository(new BinRepository());
        setBinService(new BinService(binRepository));
        start();
    }
    public void start() {
        while (true) {
            String input = new Scanner(System.in).nextLine();
            String command = Command.getCommandByString(input);
            switch (command) {
                case "create-bin": binService.create(); break;
                case "update-bin": binService.update(); break;
                case "delete-bin": binService.delete(); break;
                case "read-bin": binService.read(); break;
                case "readall-bin": binService.readAll(); break;

                case "create-product": productService.create(); break;
                case "update-product": productService.update(); break;
                case "delete-product": productService.delete(); break;
                case "read-product": productService.read(); break;
                case "readall-product": productService.readAll(); break;

                case "help": Command.view(); break;
                case "exit": System.exit(0);
            }
        }
    }
}
