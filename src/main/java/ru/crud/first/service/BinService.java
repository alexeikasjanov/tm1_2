package ru.crud.first.service;
import ru.crud.first.entity.Bin;
import ru.crud.first.entity.Product;
import ru.crud.first.repository.BinRepository;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class BinService {

    Scanner sc = new Scanner(System.in);

    private BinRepository binRepository;

    public BinService(BinRepository binRepository) {
        this.binRepository = binRepository;
    }

    public void create(){
        Bin bin = new Bin();
        binRepository.create(bin);
    }

    public void update(){
        System.out.println("Введите id обновляемого Bin");
        String id1 = sc.nextLine(); // ввожу id поля которое хочу обновить
        Bin bin = new Bin();
        binRepository.update(id1,bin);
    }

    public void delete(){
        System.out.println("введите id удаляемого Bin");
        String id = new Scanner(System.in).nextLine();
        binRepository.delete(id);
    }

    public void read(){/////
        System.out.println("введите id Bin который хотите посмотреть");
        String id = sc.nextLine();
        binRepository.read(id);
    }

    public void readAll(){
        binRepository.readAll();
    }

}
