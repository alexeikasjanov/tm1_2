package ru.crud.first.service;

import ru.crud.first.entity.Product;
import ru.crud.first.repository.ProductRepository;

import java.util.Scanner;

public class ProductService {

    Scanner sc = new Scanner(System.in);

    private ProductRepository productRepository;

    public ProductService(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    public void create(){
        System.out.println("Введите название продукта");
        String name = sc.nextLine();
        System.out.println("Введите цену продукта");
        int price = Integer.parseInt(sc.nextLine());
        System.out.println("Введите сумму продукта");
        int sum = Integer.parseInt(sc.nextLine());
        Product product = new Product(name,price,sum);
        productRepository.create(product);
        sc.reset();
    }

    public void update(){
        System.out.println("Введите id обновляемого Product");
        String id1 = sc.nextLine(); // ввожу id поля которое хочу обновить

        System.out.println("Введите новое название продукта");
        String name = sc.nextLine();

        System.out.println("Введите новую цену продукта");
        int price = Integer.parseInt(sc.nextLine());

        System.out.println("Введите новую сумму продукта");
        int sum = Integer.parseInt(sc.nextLine());

        Product product = new Product(name,price,sum);
        productRepository.update(id1,product);
        sc.reset();
    }

    public void delete(){
        System.out.println("введите id удаляемого Product");
        String id = new Scanner(System.in).nextLine();
        productRepository.delete(id);
    }

    public void read(){/////
        System.out.println("введите id Product который хотите посмотреть");
        String id = sc.nextLine();
        productRepository.read(id);
    }

    public void readAll(){
        productRepository.readAll();
    }


}
