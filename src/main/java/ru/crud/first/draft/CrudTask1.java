package ru.crud.first.draft;

import ru.crud.first.entity.Product;

import java.io.*;
import java.util.*;

public class CrudTask1 {
    public static Map<Integer, Product> map = new TreeMap<Integer, Product>();

    public static void main(String[] args) throws Exception {
        String line = "";
        String[] massive = null;
        String stroka = null;
        int id = 0; int pricemassive = 0; int summassive = 0;
        int idmassive = 0;
        System.out.println("Сначало загрузите базу. Перед выходом сохраните");
        System.out.println("");
        ArrayList<String> list = null;
        do {
            String fileName = "/Users/alekseikasyanov/Desktop/KasyanovGit/test.txt";
            String perenos = "\n";
            BufferedReader readerFile = new BufferedReader(new FileReader(fileName));
            FileOutputStream fileOutputStream = new FileOutputStream(fileName,true); // если подписать truе то запись будет поверх старых данных
            int max = map.size();
            Scanner sc = new Scanner(System.in);
            System.out.println("Введите команду (help для подсказки)");
            stroka = sc.nextLine();

            if (stroka.equals("создать")) { // СОЗДАТЬ id productName price quantity
                System.out.println("Введите название продукта");
                String name = sc.nextLine();
                System.out.println("Введите цену");
                int price = sc.nextInt();
                System.out.println("введите количество");
                int sum = sc.nextInt();
                id = max + 1;
                map.put(id, new Product(name, price, sum));
                sc.reset();
            }

            if (stroka.equals("обновить")) { // ОБНОВИТЬ id productName price quantity
                System.out.println("Обновите название для существующего продукта");
                String name1 = sc.nextLine();
                System.out.println("Обновите цену");
                int price1 = sc.nextInt();
                System.out.println("Обновите количество");
                int sum1 = sc.nextInt();
                System.out.println("Введите id обновляемого поля");
                int id1 = sc.nextInt(); // ввожу id поля которое хочу обновить
                map.remove(id1);
                map.put(id1,new Product(name1, price1, sum1));
                System.out.println("Продукт с id "+id1+" обновлен");
                sc.reset();
            }

            if (stroka.equals("удалить")) { // УДАЛИТЬ по выбору id
                System.out.println("Введите id удаляемого объекта");
                int id1 = sc.nextInt();
                map.remove(id1);
                System.out.println("Товар с id " + id1 + " удален");
                sc.reset();
            }

            if (stroka.equals("показать базу")) { // ПОКАЗАТЬ БАЗУ
                for (HashMap.Entry<Integer,Product> s : map.entrySet()){
                    System.out.println(s.getKey()+" "+s.getValue());
                }
            }

            if(stroka.equals("сохранить базу")){
                StringBuilder sb = new StringBuilder();
                for (HashMap.Entry<Integer,Product> s : map.entrySet()){
                    sb.append(s.getKey()).append(" ").append(s.getValue()).append(perenos);
                }
                String finish = sb.toString();
                try {
                    FileWriter fstream1 = new FileWriter(fileName);// конструктор с одним параметром - для перезаписи
                    BufferedWriter out1 = new BufferedWriter(fstream1); //  создаём буферезированный поток
                    out1.write(""); // очищаем, перезаписав поверх пустую строку
                    out1.close(); // закрываем
                } catch (Exception e)
                {System.err.println("Error in file cleaning: " + e.getMessage());}
                //System.out.println(finish);
                fileOutputStream.write(finish.getBytes());
            }
            fileOutputStream.close();

            if(stroka.equals("загрузить базу")){
                while(readerFile.ready()){
                    line = readerFile.readLine(); // считаю построчно
                    massive = line.split(" "); // записал строку в массив и разделил пробелом
                    idmassive = Integer.parseInt(massive[0]);
                    pricemassive = Integer.parseInt(massive[2]);
                    summassive = Integer.parseInt(massive[3]);
                    map.put(idmassive, new Product(massive[1],pricemassive,summassive));
                }
                readerFile.close();
            }
            if(stroka.equals("help")){
                System.out.println("доступные команды:");
                System.out.println("создать - создает новый объект");
                System.out.println("показать базу - показывает текущую базу");
                System.out.println("удалить - удаляет объект из базы по выбранному id");
                System.out.println("обновить - меняет поля объекта по id");
                System.out.println("сохранить базу - сохраняет выполненные действия");
                System.out.println("загрузить базу - загружает базу данных");
                System.out.println("выход - выход из программы");
            }

        } while (!stroka.equals("выход"));
    }
}
