package ru.crud.first.command;

public enum Command {
    CREATE_BIN("create-bin","[CREATE_BIN]"), CREATE_PRODUCT("create-product","[CREATE_PRODUCT]"),
    UPDATE_BIN("update-bin","[UPDATE_BIN]"),UPDATE_PRODUCT("update-product","[UPDATE_PRODUCT]"),
    DELETE_BIN("delete-bin","[DELETE_BIN]"), DELETE_PRODUCT("delete-product","[DELETE_PRODUCT]"),
    READ_BIN("read-bin","[READ_BIN]"), READ_PRODUCT("read-product","[READ_PRODUCT]"),
    READ_ALL_BIN("readall-bin","[READ_BASE_BIN]"), READ_ALL_PRODUCT("readall-product","[READ_BASE_PRODUCT]"),
    EXIT("exit","[EXIT_PROGRAMM]"),
    HELP("help","[HELP]");

    private String command;
    private String name;

    Command(String command, String name) {
        this.command = command;
        this.name = name;
    }
    public String getCommand() {
        return command;
    }
    public String getName() {
        return name;
    }

    public static String getCommandByString(String s){
        for (Command command : Command.values()){
            if (s.equals(command.getCommand())){
                return s;
            }
        }
        return null;
    }
    public static Command view(){
        System.out.println("Список доступных комманд: ");
        for (Command command : Command.values()) {
            System.out.println(command.getCommand()+" "+command.getName());
        }
        return null;
    }

}
