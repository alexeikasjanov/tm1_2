package ru.crud.first.entity;
import java.util.UUID;

public class Product {
    private String id = UUID.randomUUID().toString();
    private String name;
    private int price;
    private int sum;
    private Bin bin;

    public Product(){}

    public String getId() {
        return id;
    }
    public Bin getBin() {
        return bin;
    }
    public Product(String name, int price, int sum) {
        this.name = name;
        this.price = price;
        this.sum = sum;
   }
    public String getName() {
        return name;
    }
    public void setName(String product) {
        this.name = name;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        this.price = price;
    }
    public int getSum() {
        return sum;
    }
    public void setSum(int sum) {
        this.sum = sum;
    }

    @Override
    public String toString() {
        return name + " " + price + " "+ sum;
    }
}
