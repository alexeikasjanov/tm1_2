package ru.crud.first.entity;
import java.util.List;
import java.util.UUID;

public class Bin {
    private String id = UUID.randomUUID().toString();
    private int sum;
    private List<Product> lp;

    public Bin(){}

    public String getId() {
        return id;
    }
    public int getSum() {
        return sum;
    }
    public void setSum(int sum) {
        this.sum = sum;
    }
    public List<Product> getLp() {
        return lp;
    }
    public void setLp(List<Product> lp) {
        this.lp = lp;
    }
    @Override
    public String toString() {
        return "Bin id = "+ id  + " sum = " + sum;
    }
}
