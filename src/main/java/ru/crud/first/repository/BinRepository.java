package ru.crud.first.repository;

import ru.crud.first.entity.Bin;
import ru.crud.first.entity.Product;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class BinRepository {

    private Map<String, Bin> mapbr = new HashMap<>();

    public void create(Bin bin){
        mapbr.put(bin.getId(),bin);
        System.out.println("создан bin c id " + bin.getId());
    }

    public void update(String id,Bin bin){
        if (mapbr.containsKey(id)){
            //binRepository.getMapbr().remove(id1);
            mapbr.put(id,bin);
            System.out.println("Bin с id " + id + " обновлен");
        }
        else {
            System.out.println("нет Bin с таким ключом, введите команду повторно и проверьте ввод");
        }
    }

    public void delete(String id){
        if (mapbr.containsKey(id)){
            mapbr.remove(id);
            System.out.println("удален Bin с id "+id);
        } else {
            System.out.println("нет Bin с таким ключом, введите команду повторно и проверьте ввод");
        }
    }

    public void read(String id){/////
        if (mapbr.containsKey(id)){
            System.out.println(mapbr.get(id));
        }
        else {
            System.out.println("нет bin с таким ключом, введите команду повторно и проверьте ввод");
        }
    }
    public void readAll(){
        if (mapbr.size()>0){
            for (Map.Entry<String,Bin> s : mapbr.entrySet()){
                System.out.println("_______________________________");
                System.out.println(s.getKey()+" "+s.getValue());
            }
        }
        else {
            System.out.println("База пуста");
        }
    }

}
