package ru.crud.first.repository;

import ru.crud.first.entity.Bin;
import ru.crud.first.entity.Product;
import java.util.HashMap;
import java.util.Map;

public class ProductRepository {

    private Map<String, Product> mappr = new HashMap<>();

    public void create(Product product){
        mappr.put(product.getId(),product);
        System.out.println("создан product c id " + product.getId());
    }

    public void update(String id,Product product){
        if (mappr.containsKey(id)){
            //binRepository.getMapbr().remove(id1);
            mappr.put(id,product);
            System.out.println("product с id " + id + " обновлен");
        }
        else {
            System.out.println("нет product с таким ключом, введите команду повторно и проверьте ввод");
        }
    }

    public void delete(String id){
        if (mappr.containsKey(id)){
            mappr.remove(id);
            System.out.println("удален product с id "+id);
        } else {
            System.out.println("нет product с таким ключом, введите команду повторно и проверьте ввод");
        }
    }

    public void read(String id){/////
        if (mappr.containsKey(id)){
            System.out.println(mappr.get(id));
        }
        else {
            System.out.println("нет продукта с таким ключом, введите команду повторно и проверьте ввод");
        }
    }
    public void readAll(){
        if (mappr.size()>0){
            for (Map.Entry<String,Product> s : mappr.entrySet()){
                System.out.println("_______________________________");
                System.out.println(s.getKey()+" "+s.getValue());
            }
        }
        else {
            System.out.println("База пуста");
        }
    }

}
